# Copyright (c) 2021-2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/notification/distributed_notification_service/notification.gni")
import("//build/ohos.gni")

group("ans_targets") {
  deps = [ ":libans" ]
}

config("public_ans_config") {
  include_dirs = [
    "${services_path}/ans/include",
    "${core_path}/include",
  ]
}

ohos_shared_library("libans") {
  sanitize = {
    integer_overflow = true
    ubsan = true
    boundary_sanitize = true
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  branch_protector_ret = "pac_ret"

  shlib_type = "sa"
  version_script = "libans.map"
  include_dirs = [
    "include",
    "${ffrt_path}/interfaces/kits",
  ]

  sources = [
    "src/access_token_helper.cpp",
    "src/advanced_datashare_helper.cpp",
    "src/advanced_notification_event_service.cpp",
    "src/advanced_notification_inline.cpp",
    "src/advanced_notification_live_view_service.cpp",
    "src/advanced_notification_publish/base_publish_process.cpp",
    "src/advanced_notification_publish/common_notification_publish_process.cpp",
    "src/advanced_notification_publish/live_publish_process.cpp",
    "src/advanced_notification_publish_service.cpp",
    "src/advanced_notification_reminder_service.cpp",
    "src/advanced_notification_service.cpp",
    "src/advanced_notification_service_ability.cpp",
    "src/advanced_notification_slot_service.cpp",
    "src/advanced_notification_subscriber_service.cpp",
    "src/advanced_notification_utils.cpp",
    "src/bundle_manager_helper.cpp",
    "src/common/file_utils.cpp",
    "src/common/notification_config_parse.cpp",
    "src/distributed_device_status.cpp",
    "src/event_report.cpp",
    "src/notification_dialog.cpp",
    "src/notification_dialog_manager.cpp",
    "src/notification_extension_wrapper.cpp",
    "src/notification_local_live_view_subscriber_manager.cpp",
    "src/notification_preferences.cpp",
    "src/notification_preferences_database.cpp",
    "src/notification_preferences_info.cpp",
    "src/notification_rdb_data_mgr.cpp",
    "src/notification_slot_filter.cpp",
    "src/notification_smart_reminder/reminder_affected.cpp",
    "src/notification_smart_reminder/smart_reminder_center.cpp",
    "src/notification_subscriber_manager.cpp",
    "src/notification_timer_info.cpp",
    "src/os_account_manager_helper.cpp",
    "src/permission_filter.cpp",
    "src/reminder_config_change_observer.cpp",
    "src/reminder_data_manager.cpp",
    "src/reminder_event_manager.cpp",
    "src/reminder_swing_decision_center.cpp",
    "src/reminder_timer_info.cpp",
    "src/system_event_observer.cpp",
  ]

  configs = [ ":public_ans_config" ]

  defines = []
  cflags = []

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "../ans:ans.para",
    "../ans:ans.para.dac",
    "//third_party/icu/icu4c:shared_icuuc",
    "//third_party/libxml2:libxml2",
  ]

  if (is_double_framework) {
    cflags += [ "-DCONFIG_DUAL_FRAMEWORK" ]
  }

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  if (notification_smart_reminder_supported) {
    defines += [ "NOTIFICATION_SMART_REMINDER_SUPPORTED" ]
  }

  external_deps = [
    "ability_base:configuration",
    "ability_runtime:ability_manager",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libtokenid_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "config_policy:configpolicy_util",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "data_share:datashare_permission",
    "device_manager:devicemanagersdk",
    "ffrt:libffrt",
    "i18n:intl_util",
    "image_framework:image_native",
    "init:libbegetutil",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "relational_store:native_rdb",
    "resource_management:global_resmgr",
    "time_service:time_client",
  ]
  external_deps += component_external_deps

  if (device_usage) {
    external_deps += [ "device_usage_statistics:usagestatsinner" ]
    defines += [ "DEVICE_USAGE_STATISTICS_ENABLE" ]
  }

  if (hisysevent_usage) {
    cflags += [ "-DHAS_HISYSEVENT_PART" ]
    external_deps += [ "hisysevent:libhisysevent" ]
  }

  if (standby_enable) {
    external_deps += [ "device_standby:standby_innerkits" ]
    defines += [ "DEVICE_STANDBY_ENABLE" ]
  }

  if (player_framework) {
    external_deps += [ "player_framework:media_client" ]
    defines += [ "PLAYER_FRAMEWORK_ENABLE" ]
  }

  if (ans_hitrace_usage) {
    external_deps += [ "hitrace:hitrace_meter" ]
    defines += [ "HITRACE_METER_ENABLE" ]
  }

  if (notification_ext_feature_summary) {
    defines += [ "ENABLE_ANS_EXT_WRAPPER" ]
  }

  if (ans_config_policy_enable) {
    external_deps += [ "config_policy:configpolicy_util" ]
    defines += [ "CONFIG_POLICY_ENABLE" ]
  }

  if (screenlock_mgr_enable) {
    external_deps += [ "screenlock_mgr:screenlock_client" ]
    defines += [ "SCREENLOCK_MGR_ENABLE" ]
  }

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_prebuilt_etc("ans.para") {
  source = "etc/ans.para"
  relative_install_dir = "param"
  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_prebuilt_etc("ans.para.dac") {
  source = "etc/ans.para.dac"
  relative_install_dir = "param"
  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}
